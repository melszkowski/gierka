#include "stdafx.h"
#include "Button.h"


Button::Button(float posX, float posY, string text, ButtonType buttonType)
{
	this->posX = posX;
	this->posY = posY;
	this->text = text;

	this->width = 20.0f;
	this->height = 5.0f;

	this->buttonType = buttonType;

	this->leftX = posX - (width*0.5f);
	this->rightX = posX + (width*0.5f);

	this->topY = posY + (height*0.5f);
	this->bottomY = posY - (height*0.5f);

	color[0] = 0.3725f;
	color[1] = 0.518f;
	color[2] = 0.698f;
	color[3] = 1.0f;

	colorOfSelected[0] = 0.635f;
	colorOfSelected[1] = 0.792f;
	colorOfSelected[2] = 1.0f;
	colorOfSelected[3] = 1.0f;


	isSelected = false;
}

Button::Button() {
	this->buttonType = ButtonType::NONE;
	this->width = 0.0f;
	this->height = 0.0f;
	this->text = "";
}


Button::~Button()
{
}
