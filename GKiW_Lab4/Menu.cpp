#include "stdafx.h"
#include "Menu.h"
#include "ButtonType.h"
#include <algorithm>

Menu::Menu()
{
}


Menu::~Menu()
{
}

void Menu::loadStartButton() {
	buttons.clear();

	float posX = 0.0f;
	float posY = 20.0f;
	this->buttons.push_back(Button::Button(posX, posY, "Start", ButtonType::START));
	buttons.front().isSelected = true;
}

void Menu::loadButtons(string filename) {
	buttons.clear();
	string path = "res\\lang\\" + filename;
	ifstream input(path);

	string temp;

	float posX = 0.0f;
	float posY = 20.0f;

	//float distanceMultiplier = 1.2f;
	float distanceMultiplier = 1.0f;

	for (string line; getline(input, line); )
	{
		if (line.compare("new_game =") == 0) {
			getline(input, line);
			this->buttons.push_back(Button::Button(posX, posY, line, ButtonType::NEW_GAME));
			posY -= buttons.back().height * distanceMultiplier;

		}
		else if (line.compare("controls =") == 0) {
			getline(input, line);
			this->buttons.push_back(Button::Button(posX, posY, line, ButtonType::CONTROLS));
			posY -= buttons.back().height * distanceMultiplier;
		}
		else if (line.compare("settings =") == 0) {
			getline(input, line);
			this->buttons.push_back(Button::Button(posX, posY, line, ButtonType::SETTINGS));
			posY -= buttons.back().height * distanceMultiplier;
		}
		else if (line.compare("quit =") == 0) {
			getline(input, line);
			this->buttons.push_back(Button::Button(posX, posY, line, ButtonType::QUIT));
			posY -= buttons.back().height * distanceMultiplier;
		}

		else if (line.compare("level =") == 0) {
			getline(input, line);
			this->buttons.push_back(Button::Button(posX, posY, line, ButtonType::CHOOSE_LEVEL));
			posY -= buttons.back().height * distanceMultiplier;
		}
		else if (line.compare("gamemode =") == 0) {
			getline(input, line);
			this->buttons.push_back(Button::Button(posX, posY, line, ButtonType::CHOOSE_GAMEMODE));
			posY -= buttons.back().height * distanceMultiplier;
		}


	}
	buttons.front().isSelected = true;

}

string Menu::getLevelFilename(string levelname) {
	
	string path = "res\\lang\\levels.m";
	ifstream input(path);

	string temp;

	for (string line; getline(input, line); )
	{
		if (line.compare("level =") == 0) {
			getline(input, line);
			if (line.compare(levelname) == 0) {
				for (line; getline(input, line);) {
					if (line.compare("file =") == 0) {
						getline(input, line);
						return line;
					}
				}
			}
		}
	}

}

Button Menu::getSelectedButton() {
	if (!buttons.empty()) {
		for (list<Button>::iterator bi = buttons.begin(); bi != buttons.end(); bi++) {
			if (bi->isSelected) {
				return *bi;
			}
		}
	}
	return Button();

}

void Menu::selectNext() {
	if (!buttons.empty()) {

		int counter = 1;
		for (list<Button>::iterator bi = buttons.begin(); bi != buttons.end(); ++bi) {
			if (bi->isSelected) {
				bi->isSelected = false;

				if (counter < buttons.size()) {
					++bi;
					bi->isSelected = true;
				}
				else {
					buttons.front().isSelected = true;
				}
				return;

			}

			counter++;
		}
	}
}

void Menu::selectPrevious() {
	if (!buttons.empty()) {
		int counter = 1;
		for (list<Button>::reverse_iterator rbi = buttons.rbegin(); rbi != buttons.rend(); ++rbi) {
			if (rbi->isSelected) {
				rbi->isSelected = false;

				if (counter < buttons.size()) {
					++rbi;
					rbi->isSelected = true;
				}
				else {
					buttons.back().isSelected = true;
				}
				return;
			}

			counter++;
		}
	}
}

