#include "stdafx.h"
#include "Camera.h"
#include "MyMath.h"

Camera::Camera()
{
}


Camera::~Camera()
{
}

void Camera::increaseZoom() {
	if (distanceFromPlayer > minDistanceFromPlayer)
		distanceFromPlayer -= 0.1f;
}

void Camera::decreaseZoom() {
	if (distanceFromPlayer < maxDistanceFromPlayer)
		distanceFromPlayer += 0.1f;

}

float Camera::calculateHorizontalDistance() {

	float horizontalDistance = distanceFromPlayer * cos(MyMath::degreesToRadians(pitch));
	return horizontalDistance;
}

float Camera::calculateVerticalDistance() {
	float verticalDistance = distanceFromPlayer * sin(MyMath::degreesToRadians(pitch));
	return verticalDistance;
}

////TODO
//bool Camera::isCameraCollided() {
//	if ((this->pos.x > 16) || (this->pos.x < -16) || (this->pos.z > 14) || (this->pos.z < -14) || (this->pos.y > 20) || (this->pos.y < 0))
//		return true;
//	else
//		return false;
//}

//invoked when camera collides
void Camera::handleCameraCollision(Player player) {
	if ((distanceFromPlayer > minDistanceFromPlayer)) {
		increaseZoom();
		setCamera(player);
	}
}

void Camera::setCamera(Player player) {
	pos.y = player.pos.y + calculateVerticalDistance();

	player.calculateRotationY();

	float xCameraOffset = distanceFromPlayer * sin(player.rotationY);
	float zCameraOffset = distanceFromPlayer * cos(player.rotationY);
	float horizontalDistance = calculateHorizontalDistance();
	float verticalDistance = calculateVerticalDistance();
	this->pos.x = player.pos.x - xCameraOffset;
	this->pos.z = player.pos.z - zCameraOffset;

	this->dir.x = player.dir.x;
	this->dir.y = player.dir.y;
	this->dir.z = player.dir.z;
}

void Camera::updateCamera(Player player) {

	setCamera(player);

	if (distanceFromPlayer < maxDistanceFromPlayer) {
		decreaseZoom();
		setCamera(player);
	}

}


