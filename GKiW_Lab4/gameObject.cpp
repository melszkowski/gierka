#include "stdafx.h"
#include "gameObject.h"
#include <string>

using namespace std;


gameObject::gameObject(float posX, float posY, float posZ, string name) {
	this->posX = posX;
	this->posY = posY;
	this->posZ = posZ;
	this->name = name;
}

gameObject::gameObject()
{
	model = 0;
	texture = 0;
}


gameObject::~gameObject()
{
}

gameObject gameObject::clone() {
	gameObject clone(this->posX, this->posY, this->posZ, this->name);
	clone.model = this->model;
	clone.texture = this->texture;
	clone.collisionShapes = this->collisionShapes;
	for (int i = 0; i < 4; i++) {
		clone.amb[i] = this->amb[i];
		clone.dif[i] = this->dif[i];
		clone.spe[i] = this->spe[i];
	}
	clone.shininess = this->shininess;
	return clone;
}