#include "stdafx.h"
#include "MyMath.h"


MyMath::MyMath()
{
}


MyMath::~MyMath()
{
}


float MyMath::radiansToDegrees(float angle) {
	return (angle * 180 / PI);
}

float MyMath::degreesToRadians(float angle) {
	return (angle * PI / 180);
}


bool MyMath::doesPointBelongToLineSegment(float x, float y1, float y2) {
	if ((x >= y1) && (x <= y2)) {
		return true;
	}
	else {
		return false;
	}
}

bool MyMath::doesPointBelongToBoundingBox(vec3 point, vec3 bbCenter, vec3 bbSize) {
	vec3 bbExtents;
	bbExtents.x = bbSize.x * 0.5f;
	bbExtents.y = bbSize.y * 0.5f;
	bbExtents.z = bbSize.z * 0.5f;

	if (doesPointBelongToLineSegment(point.x, bbCenter.x - bbExtents.x, bbCenter.x + bbExtents.x)) {
		if (doesPointBelongToLineSegment(point.y, bbCenter.y - bbExtents.y, bbCenter.y + bbExtents.y)) {
			if (doesPointBelongToLineSegment(point.z, bbCenter.z - bbExtents.z, bbCenter.z + bbExtents.z)) {
				return true;
			}
		}
	}
		return false;
}

bool MyMath::doLineSegmentsIntersect(float x1, float x2, float y1, float y2)
{
	//NOTE: x1 < x2 and y1 < y2
	if ((x2 >= y1) && y2 >= x1) {
		return true;
	}
	else {
		return false;
	}
}

bool MyMath::doBoundingBoxesIntersect(vec3 bb1Center, vec3 bb1Size, vec3 bb2Center, vec3 bb2Size)
{
	vec3 b1Extents;
	vec3 b2Extents;
	b1Extents.x = bb1Size.x * 0.5f;
	b1Extents.y = bb1Size.y * 0.5f;
	b1Extents.z = bb1Size.z * 0.5f;

	b2Extents.x = bb2Size.x * 0.5f;
	b2Extents.y = bb2Size.y * 0.5f;
	b2Extents.z = bb2Size.z * 0.5f;

	// check if bounding boxes intersect in X, Y and Z axis
	if (doLineSegmentsIntersect(bb1Center.x - b1Extents.x, bb1Center.x + b1Extents.x, bb2Center.x - b2Extents.x, bb2Center.x + b2Extents.x)) {
		if (doLineSegmentsIntersect(bb1Center.y - b1Extents.y, bb1Center.y + b1Extents.y, bb2Center.y - b2Extents.y, bb2Center.y + b2Extents.y)) {
			if (doLineSegmentsIntersect(bb1Center.z - b1Extents.z, bb1Center.z + b1Extents.z, bb2Center.z - b2Extents.z, bb2Center.z + b2Extents.z)) {
				return true;
			}
		}
	}
		return false;
}
