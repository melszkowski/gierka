#include "stdafx.h"
#include "BoundingBox.h"


BoundingBox::BoundingBox()
{
	
}

BoundingBox::BoundingBox(float posX, float posY, float posZ, float width, float height, float depth)
{
	this->center.x = posX;
	this->center.y = posY;
	this->center.z = posZ;

	this->size.x = width;
	this->size.z = depth;
	this->size.y = height;
}

 
BoundingBox::~BoundingBox()
{
}

BoundingBox BoundingBox::createBoundingBoxFromString(string line) {

	float x, y, z, width, height, depth, areLegs;
	x = 0;
	y = 0;
	z = 0;
	width = 0;
	height = 0;
	depth = 0;
	string::size_type sz;

	x = stof(line, &sz);
	line = line.substr(sz);
	sz = 0;

	y = stof(line, &sz);
	line = line.substr(sz);
	sz = 0;

	z = stof(line, &sz);
	line = line.substr(sz);
	sz = 0;

	width = stof(line, &sz);
	line = line.substr(sz);
	sz = 0;

	height = stof(line, &sz);
	line = line.substr(sz);
	sz = 0;

	depth = stof(line, &sz);
	line = line.substr(sz);
	sz = 0;

	areLegs = stof(line, &sz);


	BoundingBox bb(x, y, z, width, height, depth);
	if (areLegs) {
		bb.areLegs = true;
	}
	return bb;
}

void BoundingBox::setPos(float posX, float posY, float posZ) {
	this->center.x = posX;
	this->center.y = posY;
	this->center.z = posZ;

}