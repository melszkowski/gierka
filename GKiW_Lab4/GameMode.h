#pragma once
enum class GameMode {
	NONE = 0, 
	COLLECT,
	RACE,
	DESTROY
 };