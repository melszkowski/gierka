#pragma once

#include "Button.h"

#include <list>
#include <memory>
class Menu
{
public:
	list<Button> buttons;

	Menu();
	~Menu();
	void loadStartButton();
	void loadButtons(string filename);
	string getLevelFilename(string levelname);
	Button getSelectedButton();
	void selectNext();
	void selectPrevious();
};

