#pragma once
enum class ButtonType {
	NONE = 0,
	NEW_GAME,
	CONTROLS,
	SETTINGS,
	QUIT,
	CHOOSE_LEVEL,
	CHOOSE_GAMEMODE,
	START
};