#pragma once

#include "BoundingBox.h"
#include <list>
#include <string>
#include <memory>
using namespace std;

class Player
{
public:
	vec3 pos;
	vec3 dir;

	vec3 startingPos;
	vec3 startingDir;

	GLuint playerLoadedObject;
	GLuint playerTexture;

	GLuint mainPropeller;
	GLuint secondPropeller;

	float maxVelX;
	float maxVelZ;
	float maxVelY;
	float maxVelYDown;
	float maxVelRY;

	float deltaVelX, deltaVelZ, deltaVelY, deltaVelRY;

	float velRY, velZ, velX, velY;
	float rotationY;

	float propellerSpeed = 0;
	float propellerMaxSpeed = 2.0f;
	float propellerMinSpeed = 0.2f;
	float propellerRotation = 0;

	list<BoundingBox> collisionShapes;

	int itemsCollected;

	Player();
	~Player();
	void setPlayer(string playerSettingsFilename);
	void updateBoundingBoxesPositions();
	void handleChanges();
	void calculateRotationY();
	void increaseVelX();
	void decreaseVelX();
	void increaseVelZ();
	void decreaseVelZ();
	void increaseVelY();
	void decreaseVelY();
	void increaseVelRY();
	void decreaseVelRY();
	void calculatePropellerRotation();
	void simulateGravity();
	BoundingBox createBoundingBoxFromString(string line);
	void extractAndSetVelocities(string line);
	void loadPlayerInfo(string fileName);
	void reset();
};

