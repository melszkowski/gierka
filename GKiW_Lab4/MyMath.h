#pragma once

#include "BoundingBox.h"

class MyMath
{
public:
	MyMath();
	~MyMath();
	static float radiansToDegrees(float angle);
	static float degreesToRadians(float angle);
	static bool doLineSegmentsIntersect(float x1, float x2, float y1, float y2);
	static bool doesPointBelongToLineSegment(float x, float y1, float y2);
	static bool doesPointBelongToBoundingBox(vec3 point, vec3 bbSize, vec3 bbCenter);
	static bool doBoundingBoxesIntersect(vec3 bb1Center, vec3 bb1Size, vec3 bb2Center, vec3 bb2Size);
};

