#pragma once
#include "gameObject.h"
class CollectibleObject :
	public gameObject
{
public:
	static BoundingBox boundingBox;
	static int initialNumberOfElements;

	CollectibleObject();
	~CollectibleObject();
};

