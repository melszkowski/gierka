#pragma once

#include <math.h>
#include "Player.h"
#include "stdafx.h"

class Camera
{
public:

	vec3 pos;
	vec3 dir;

	float distanceFromPlayer = 5.0f;
	float maxDistanceFromPlayer = 5.0f;
	float minDistanceFromPlayer = 0.6f;
	float angleAroundPlayer = 0;
	float pitch = 20;
	float yaw = 0;
	float roll = 0;

	void increaseZoom();
	void decreaseZoom();



	float calculateHorizontalDistance();
	float calculateVerticalDistance();
	//bool isCameraCollided();
	void handleCameraCollision(Player player);
	void setCamera(Player player);
	void updateCamera(Player player);

	Camera();
	~Camera();
};

