#pragma once

#include <string>
#include "BoundingBox.h"
#include <list>

using namespace std;

class gameObject
{


public:
	float posX;
	float posY;
	float posZ;
	vec3 rotation;
	string name;
	
	GLuint model;
	GLuint texture;

	list<BoundingBox> collisionShapes;
	
	float amb[4] = { 1.0f, 1.0f, 1.0f, 1.0f };
	float dif[4] = { 1.0f, 1.0f, 1.0f, 1.0f };
	float spe[4] = { 1.0f, 1.0f, 1.0f, 1.0f };

	float shininess = 5.0;

public:
	gameObject(float posX, float posY, float posZ, string name);
	gameObject();
	~gameObject();
	gameObject clone();
};

