#pragma once

#include <list>
#include <math.h>
#include <memory>

#include "gameObject.h"
#include "Player.h"
#include "CollectibleObject.h"
#include "GameMode.h"
#include "Camera.h"
#include "Lamp.h"

class Game
{
public:
	Player player;
	Camera camera;
	//list of objects that create scene
	list<gameObject> scene;
	list<Lamp> lighting;

	//list of objects, that can be collected - optional
	list<CollectibleObject> collectibles;
	int initialNumberOfCollectibles = 0;
	int timeToCollect = 0;
	int initialTimeToCollect = 0;

	GameMode gameMode;

	string levelFilename = "";
	string lightsFilename = "";
	string collectiblesFilename = "";
	bool fogActive = false;

	void handleCollisions();
	
	Game();
	~Game();
	void loadObjectFromFile(string filename);
	void loadLevel(string filename);
	void loadCollectibles(string filename);

	void loadLightSources(string filename);

	void handleChanges();

	void notifyTenthOfSec();

	void reset();

	static GameMode getGameModeFromString(string gm);
};

