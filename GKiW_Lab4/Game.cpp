#include "stdafx.h"
#include "Game.h"
#include "ObjLoader.h"
#include "MyMath.h"

Game::Game()
{
}


Game::~Game()
{
	//player.~Player();
	//camera.~Camera();
	//for (list<gameObject>::iterator it = scene.begin(); it != scene.end(); ++it) {
	//	it = scene.erase(it);

	//}

	//while (!scene.empty()) {
	//	 
	//	scene.front().~gameObject();
	//	scene.pop_front();
	//}
	//scene.clear();
}

void Game::loadObjectFromFile(string filename) {
	cout << "Loading file: " << filename << endl;
	string path = "res\\gameObjects\\" + filename;
	ifstream input(path);

	ObjLoader objLoader;
	string temp;
	BoundingBox bb;

	gameObject gO;

	bool areRotated180 = false;
	bool areRotated90 = false;

	for (string line; getline(input, line); )
	{

		if (line.at(0) == '#') {
			continue;
		}
		else if (line.compare("name =") == 0) {
			getline(input, line);
			gO.name = line;
		}
		else if (line.compare("model =") == 0) {
			getline(input, line);
			temp = "res\\models\\" + line;
			gO.model = objLoader.LoadObj(temp.c_str());
		}
		else if (line.compare("texture =") == 0) {
			getline(input, line);
			temp = "res\\textures\\" + line;
			gO.texture = objLoader.LoadTexture(temp.c_str());
		}
		else if (line.compare("ambient =") == 0) {
			getline(input, line);
			string::size_type sz;

			gO.amb[0] = stof(line, &sz);
			line = line.substr(sz);
			sz = 0;

			gO.amb[1] = stof(line, &sz);
			line = line.substr(sz);
			sz = 0;

			gO.amb[2] = stof(line, &sz);
			line = line.substr(sz);
			sz = 0;

			gO.amb[3] = stof(line, &sz);
		}

		else if (line.compare("diffuse =") == 0) {
			getline(input, line);
			string::size_type sz;

			gO.dif[0] = stof(line, &sz);
			line = line.substr(sz);
			sz = 0;

			gO.dif[1] = stof(line, &sz);
			line = line.substr(sz);
			sz = 0;

			gO.dif[2] = stof(line, &sz);
			line = line.substr(sz);
			sz = 0;

			gO.dif[3] = stof(line, &sz);
		}

		else if (line.compare("specular =") == 0) {
			getline(input, line);
			string::size_type sz;

			gO.spe[0] = stof(line, &sz);
			line = line.substr(sz);
			sz = 0;

			gO.spe[1] = stof(line, &sz);
			line = line.substr(sz);
			sz = 0;

			gO.spe[2] = stof(line, &sz);
			line = line.substr(sz);
			sz = 0;

			gO.spe[3] = stof(line, &sz);
		}

		else if (line.compare("shininess =") == 0) {
			getline(input, line);
			string::size_type sz;
			gO.shininess = stof(line, &sz);
		}
		else if (line.compare("rot =") == 0) {
			getline(input, line);
			string::size_type sz;

			gO.rotation.x = stof(line, &sz);
			line = line.substr(sz);
			sz = 0;

			gO.rotation.y = stof(line, &sz);
			line = line.substr(sz);
			sz = 0;

			gO.rotation.z = stof(line, &sz);

		}
		else if (line.compare("bounding box:") == 0) {
			getline(input, line);
			BoundingBox bb = BoundingBox::createBoundingBoxFromString(line);
			gO.collisionShapes.push_back(bb);
		}
		else if (line.compare("pos =") == 0) {
			getline(input, line);
			string::size_type sz;

			gO.posX = stof(line, &sz);
			line = line.substr(sz);
			sz = 0;

			gO.posY = stof(line, &sz);
			line = line.substr(sz);
			sz = 0;

			gO.posZ = stof(line, &sz);

			//rotate bounding box together with object (only 90/180/270 degrees rotations in y axis)
			if (((gO.rotation.y == 90.0f || gO.rotation.y == 270.0f) && !areRotated90) || (gO.rotation.y == 0.0f && areRotated90)) {
				areRotated90 = !areRotated90;

				float temp;

				for (list<BoundingBox>::iterator bbIterator = gO.collisionShapes.begin(); bbIterator != gO.collisionShapes.end(); ++bbIterator) {
					temp = bbIterator->size.x;
					bbIterator->size.x = bbIterator->size.z;
					bbIterator->size.z = temp;

				}
			}

			//if should be rotated, and haven't been yet - rotate.
			// if shouldn't be rotated and already have been - rotate again.
			if ((gO.rotation.y == 180.0f && !areRotated180) || (gO.rotation.y == 0.0f && areRotated180)) {
				areRotated180 = !areRotated180;
				for (list<BoundingBox>::iterator bbIterator = gO.collisionShapes.begin(); bbIterator != gO.collisionShapes.end(); ++bbIterator) {

					bbIterator->center.x *= -1.0f;
					bbIterator->center.z *= -1.0f;
				}
			}

			this->scene.push_back(gO);

		}
	}
}

void Game::loadLevel(string filename) {
	this->scene.clear();

	string path = "res\\lvl\\" + filename;
	ifstream input(path);

	ObjLoader objLoader;
	string temp;


	for (string line; getline(input, line); )
	{
		if (line.compare("player_pos =") == 0) {
			getline(input, line);
			string::size_type sz;

			this->player.startingPos.x = stof(line, &sz);
			line = line.substr(sz);
			sz = 0;

			this->player.startingPos.y = stof(line, &sz);
			line = line.substr(sz);
			sz = 0;

			this->player.startingPos.z = stof(line, &sz);
		}
		else if (line.compare("player_dir =") == 0) {
			getline(input, line);
			string::size_type sz;

			this->player.startingDir.x = stof(line, &sz);
			line = line.substr(sz);
			sz = 0;

			this->player.startingDir.y = stof(line, &sz);
			line = line.substr(sz);
			sz = 0;

			this->player.startingDir.z = stof(line, &sz);
		}
		else if (line.compare("lightsFilename =") == 0) {
			getline(input, line);
			this->lightsFilename = line;
		}
		else if (line.compare("collectiblesFilename =") == 0) {
			getline(input, line);
			this->collectiblesFilename = line;
		}
		else if (line.compare("fog") == 0) {
			this->fogActive = true;
		}
		else {
			loadObjectFromFile(line);
		}

	}

}


void Game::handleCollisions() {


	if (!scene.empty()) {
		//for every object present on the scene...
		for (list<gameObject>::iterator it = scene.begin(); it != scene.end(); ++it) {

			if (!it->collisionShapes.empty()) {

				//for every bounding box of specific object...
				for (list<BoundingBox>::iterator objectBbIterator = it->collisionShapes.begin(); objectBbIterator != it->collisionShapes.end(); ++objectBbIterator) {

					vec3 actualObjectBbPos;
					actualObjectBbPos.x = it->posX + objectBbIterator->center.x;
					actualObjectBbPos.y = it->posY + objectBbIterator->center.y;
					actualObjectBbPos.z = it->posZ + objectBbIterator->center.z;

					//camera collision detection
					while (MyMath::doesPointBelongToBoundingBox(this->camera.pos, actualObjectBbPos, objectBbIterator->size)) {
						camera.handleCameraCollision(this->player);
					}

					if (!this->player.collisionShapes.empty()) {

						//and for every bounding box of player...
						//check for collisions
						for (list<BoundingBox>::iterator playerBbIterator = this->player.collisionShapes.begin(); playerBbIterator != player.collisionShapes.end(); ++playerBbIterator) {

							vec3 actualPlayerBbPos;
							actualPlayerBbPos.x = playerBbIterator->center.x + this->player.pos.x;
							actualPlayerBbPos.y = playerBbIterator->center.y + this->player.pos.y;
							actualPlayerBbPos.z = playerBbIterator->center.z + this->player.pos.z;

							if (MyMath::doBoundingBoxesIntersect(actualObjectBbPos, objectBbIterator->size, actualPlayerBbPos, playerBbIterator->size)) {
								
								if (it->name.compare("water") == 0) {
									this->reset();

								}

								if (playerBbIterator->areLegs) {
									this->player.velX = 0.0f;
									this->player.velY = 0.0f;
									this->player.velZ = 0.0f;
									this->player.velRY = 0.0f;
									this->player.pos.y = actualObjectBbPos.y + (objectBbIterator->size.y * 0.5f) - 0.01f;
									
								}
								else {
									this->reset();
								}
								return;
							}
						}
					}

				}
			}
		}
	}

	if (!collectibles.empty()) {

		CollectibleObject cO = collectibles.front();

		vec3 actualObjectBbPos;
		actualObjectBbPos.x = cO.posX;
		actualObjectBbPos.y = cO.posY;
		actualObjectBbPos.z = cO.posZ;

		if (!this->player.collisionShapes.empty()) {

			//and for every bounding box of player...
			//check for collisions
			for (list<BoundingBox>::iterator playerBbIterator = this->player.collisionShapes.begin(); playerBbIterator != player.collisionShapes.end(); ++playerBbIterator) {

				vec3 actualPlayerBbPos;
				actualPlayerBbPos.x = playerBbIterator->center.x + this->player.pos.x;
				actualPlayerBbPos.y = playerBbIterator->center.y + this->player.pos.y;
				actualPlayerBbPos.z = playerBbIterator->center.z + this->player.pos.z;

				if (MyMath::doBoundingBoxesIntersect(actualObjectBbPos, cO.boundingBox.size, actualPlayerBbPos, playerBbIterator->size)) {
					collectibles.pop_front();
					this->player.itemsCollected++;
				}



			}
		}
	}



}

void Game::loadCollectibles(string filename) {
	this->collectibles.clear();
	if (filename.compare("") == 0) {
		return;
	}
	initialNumberOfCollectibles = 0;
	player.itemsCollected = 0;

	string path = "res\\gameObjects\\" + filename;
	ifstream input(path);

	ObjLoader objLoader;
	string temp;
	BoundingBox bb;

	CollectibleObject cO;

	for (string line; getline(input, line); )
	{

		if (line.at(0) == '#') {
			continue;
		}

		else if (line.compare("model =") == 0) {
			getline(input, line);
			temp = "res\\models\\" + line;
			cO.model = objLoader.LoadObj(temp.c_str());
		}

		else if (line.compare("ambient =") == 0) {
			getline(input, line);
			string::size_type sz;

			cO.amb[0] = stof(line, &sz);
			line = line.substr(sz);
			sz = 0;

			cO.amb[1] = stof(line, &sz);
			line = line.substr(sz);
			sz = 0;

			cO.amb[2] = stof(line, &sz);
			line = line.substr(sz);
			sz = 0;

			cO.amb[3] = stof(line, &sz);
		}

		else if (line.compare("diffuse =") == 0) {
			getline(input, line);
			string::size_type sz;

			cO.dif[0] = stof(line, &sz);
			line = line.substr(sz);
			sz = 0;

			cO.dif[1] = stof(line, &sz);
			line = line.substr(sz);
			sz = 0;

			cO.dif[2] = stof(line, &sz);
			line = line.substr(sz);
			sz = 0;

			cO.dif[3] = stof(line, &sz);
		}

		else if (line.compare("specular =") == 0) {
			getline(input, line);
			string::size_type sz;

			cO.spe[0] = stof(line, &sz);
			line = line.substr(sz);
			sz = 0;

			cO.spe[1] = stof(line, &sz);
			line = line.substr(sz);
			sz = 0;

			cO.spe[2] = stof(line, &sz);
			line = line.substr(sz);
			sz = 0;

			cO.spe[3] = stof(line, &sz);
		}

		else if (line.compare("shininess =") == 0) {
			getline(input, line);
			string::size_type sz;
			cO.shininess = stof(line, &sz);
		}
		else if (line.compare("time_to_collect =") == 0) {
			getline(input, line);
			string::size_type sz;
			// assign in miliseconds:
			this->timeToCollect = stof(line, &sz) * 1000;
			this->initialTimeToCollect = timeToCollect;
		}
		else if (line.compare("bounding box:") == 0) {
			getline(input, line);
			cO.boundingBox = BoundingBox::createBoundingBoxFromString(line);
		}
		else if (line.compare("pos =") == 0) {
			getline(input, line);

			string::size_type sz;

			cO.posX = stof(line, &sz);
			line = line.substr(sz);
			sz = 0;

			cO.posY = stof(line, &sz);
			line = line.substr(sz);
			sz = 0;

			cO.posZ = stof(line, &sz);
			this->collectibles.push_back(cO);
			this->initialNumberOfCollectibles++;
		}
	}
}

void Game::loadLightSources(string filename) {
	
	this->lighting.clear();

		string path = "res\\gameObjects\\" + filename;
		ifstream input(path);

		string temp;

		Lamp lamp;

		for (string line; getline(input, line); )
		{

			if (line.at(0) == '#') {
				continue;
			}

			else if (line.compare("ambient =") == 0) {
				getline(input, line);
				string::size_type sz;

				lamp.amb[0] = stof(line, &sz);
				line = line.substr(sz);
				sz = 0;

				lamp.amb[1] = stof(line, &sz);
				line = line.substr(sz);
				sz = 0;

				lamp.amb[2] = stof(line, &sz);
			}

			else if (line.compare("diffuse =") == 0) {
				getline(input, line);
				string::size_type sz;

				lamp.dif[0] = stof(line, &sz);
				line = line.substr(sz);
				sz = 0;

				lamp.dif[1] = stof(line, &sz);
				line = line.substr(sz);
				sz = 0;

				lamp.dif[2] = stof(line, &sz);
			}

			else if (line.compare("specular =") == 0) {
				getline(input, line);
				string::size_type sz;

				lamp.spe[0] = stof(line, &sz);
				line = line.substr(sz);
				sz = 0;

				lamp.spe[1] = stof(line, &sz);
				line = line.substr(sz);
				sz = 0;

				lamp.spe[2] = stof(line, &sz);
			}
			else if (line.compare("pos =") == 0) {
				getline(input, line);

				string::size_type sz;

				lamp.pos[0] = stof(line, &sz);
				line = line.substr(sz);
				sz = 0;

				lamp.pos[1] = stof(line, &sz);
				line = line.substr(sz);
				sz = 0;

				lamp.pos[2] = stof(line, &sz);
				line = line.substr(sz);
				sz = 0;

				lamp.pos[3] = stof(line, &sz);
				this->lighting.push_back(lamp);

			}
		}
}

void Game::handleChanges() {
	this->player.handleChanges();
}

void Game::notifyTenthOfSec() {
	if (gameMode == GameMode::COLLECT && timeToCollect > 0) {
		this->timeToCollect -= 100;
	}

}

void Game::reset() {
	this->player.reset();
	if (this->gameMode == GameMode::COLLECT) {
		timeToCollect = initialTimeToCollect;
		loadCollectibles(this->collectiblesFilename);
	}
}

GameMode Game::getGameModeFromString(string gm)
{
	if (gm.compare("Checkpoints") == 0) {
		return GameMode::COLLECT;
	}
	else if (gm.compare("Freeplay") == 0) {
		return GameMode::NONE;
	}
	else if (gm.compare("Destroy") == 0) {
		return GameMode::DESTROY;
	}
	else return GameMode::NONE;
}
