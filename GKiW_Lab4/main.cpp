#include "stdafx.h"

#include "Menu.h"
#include "Game.h"
#include "ObjLoader.h"
#include "MyMath.h"
#include "GeneralState.h"

using namespace std;

#pragma region Zmienne globalne

double timeElapsed = 0.0;

int mouseX = 0; // aktualna pozycja kursora myszy (x)
int mouseY = 0; // aktualna pozycja kursora myszy (y)
string toPrintAtCenter = "";


bool captureMouse = false; // czy przechwytywa� kursor myszy?
bool debugMode = false;

float mouseSensitivity = .15f; // czu�o�� na ruchy kursora myszy

Game game;
Menu menu;

GeneralState generalState = GeneralState::NONE;

#pragma endregion

vec3 loadSettingsReturnResolution();
void handleButton(Button button);
void initGame(string levelFilename, string playerFilename, GameMode gameMode);
void initMenu();
void drawGLString(GLint posX, GLint posY, string textString, float redFactor, float greenFactor, float blueFactor);
void clearStringPrintedAtCenter(int id);

int currentTime = 0;
int oldTime = 0;
int deltaTime = 0;
float fps = 0;

int main(int argc, char* argv[])
{

	ObjLoader objLoader;
	vec3 windowSize = loadSettingsReturnResolution();

	glutInit(&argc, argv);

	glutInitWindowPosition(5, 5);
	glutInitWindowSize(windowSize.x, windowSize.y);

	glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH);

	glutCreateWindow("Gierka");

	// enabling alpha channel for transparency
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glEnable(GL_BLEND);

	initMenu();

	glutReshapeFunc(onReshape);
	glutKeyboardUpFunc(onKeyUp);
	glutPassiveMotionFunc(OnMouseMove);
	glutMotionFunc(OnMouseMove);

	glutIgnoreKeyRepeat(true);

	glEnable(GL_DEPTH_TEST);

	glEnable(GL_CULL_FACE); // W��czenie cullingu - rysowania tylko jednej strony wielok�t�w
	glCullFace(GL_BACK); // Okre�lenie, kt�r� stron� wielok�t�w chcemy ukrywa�
	glFrontFace(GL_CCW); // Okre�lenie, jaki kierunek definicji wierzcho�k�w oznacza prz�d wielok�tu (GL_CCW - przeciwnie do ruchu wskaz�wek zegara, GL_CW - zgodnie)


	// Ustawienie obs�ugi myszy
	glutWarpPointer(glutGet(GLUT_WINDOW_WIDTH) / 2, glutGet(GLUT_WINDOW_HEIGHT) / 2); // Przesuni�cie kursora na �rodek ekranu
	mouseX = glutGet(GLUT_WINDOW_WIDTH) / 2;
	mouseY = glutGet(GLUT_WINDOW_HEIGHT) / 2;

	glutSetCursor(GLUT_CURSOR_NONE); // Ukrycie kursora

	glutMainLoop();

	return 0;
}

/////////////////////

#pragma region Obsluga wejscia

bool keystate[255];

void gameOnKeyPress(unsigned char key, int x, int y) {
	if (!keystate[key]) {
		keystate[key] = true;
		gameOnKeyDown(key, x, y);
	}
}

void menuOnKeyPress(unsigned char key, int x, int y) {
	if (!keystate[key]) {
		keystate[key] = true;
		menuOnKeyDown(key, x, y);
	}
}

void gameOnKeyDown(unsigned char key, int x, int y) {
	if (key == ESC) {
		initMenu();
	}
	if (key == 'b' || key == 'B') {
		debugMode = !debugMode;
	}

	if (key == 'l' || key == 'L') {
		cout << game.player.pos.x << " " << game.player.pos.y << " " << game.player.pos.z << endl;
	}

}

void menuOnKeyDown(unsigned char key, int x, int y)
{
	if (key == ESC) {
		//just load primary screen (main menu):
		menu.loadButtons("main.m");
		//glutLeaveMainLoop();
	}

	if (key == ENTER) {
		handleButton(menu.getSelectedButton());
	}
}

void onKeyUp(unsigned char key, int x, int y) {
	keystate[key] = false;
}

// Zapami�tanie pozycji kursora myszy w momencie, gdy nast�puje jego przesuni�cie.
// Zapami�tana pozycja jest p�niej "konsumowana" przez OnTimer().
void OnMouseMove(int x, int y) {
	mouseX = x;
	mouseY = y;
}

#pragma endregion

void onTimer(int id) {
	if (generalState == GeneralState::GAME) {
		glutTimerFunc(17, onTimer, 0);
	}
	else {
		return;
	}

#pragma region Obsluga klawiszy

	//if (captureMouse) {
	//	game.player.velRY = -mouseSensitivity * (glutGet(GLUT_WINDOW_WIDTH) / 2 - mouseX);

	//	//changing camera via mouse in X axis disabled, uncomment following line to enable
	//	//game.player.velRX = mouseSensitivity * (glutGet(GLUT_WINDOW_HEIGHT) / 2 - mouseY);
	//	glutWarpPointer(glutGet(GLUT_WINDOW_WIDTH) / 2, glutGet(GLUT_WINDOW_HEIGHT) / 2);
	//}

	if (keystate['w']) {
		game.player.increaseVelY();
	}
	if (keystate['s']) {
		game.player.decreaseVelY();
	}
	if (keystate['a']) {
		game.player.decreaseVelRY();
	}
	if (keystate['d']) {
		game.player.increaseVelRY();
	}
	if (keystate['5']) {
		game.player.decreaseVelZ();
	}
	if (keystate['8']) {
		game.player.increaseVelZ();
	}
	if (keystate['4']) {
		game.player.decreaseVelX();
	}
	if (keystate['6']) {
		game.player.increaseVelX();
	}

	if (keystate['f']) {
		game.player.velRY = 0.0f;
		game.player.velY = 0.0f;
		game.player.velX = 0.0f;
		game.player.velZ = 0.0f;

	}

#pragma endregion

	game.handleChanges();
	
	if (game.timeToCollect == 0 && game.gameMode == GameMode::COLLECT) {
		toPrintAtCenter = "GAME OVER";
		glutTimerFunc(3000, clearStringPrintedAtCenter, 0);
		game.reset();
	}
	if (game.collectibles.size() == 0 && game.gameMode == GameMode::COLLECT) {
		toPrintAtCenter = "YOU'VE WON!";
		glutTimerFunc(3000, clearStringPrintedAtCenter, 0);
		game.reset();
	}
}

void tenthOfSec(int id) {
	if (generalState == GeneralState::GAME) {

		glutTimerFunc(100, tenthOfSec, 0);
		game.notifyTenthOfSec();
	}
}

void menuSpecialKeyPress(int key, int x, int y)
{
	if (key == GLUT_KEY_UP) {
		menu.selectPrevious();

	}

	if (key == GLUT_KEY_DOWN) {
		menu.selectNext();
	}

}

void drawGLString(GLint posX, GLint posY, string textString, float redFactor, float greenFactor, float blueFactor)
{
	int len = textString.length();
	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_SCREEN_WIDTH), 0, glutGet(GLUT_SCREEN_HEIGHT));

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();

	glLoadIdentity();
	glDisable(GL_LIGHTING);

	glColor3f(redFactor, greenFactor, blueFactor);

	glRasterPos2i(posX, posY);
	for (int i = 0; i < len; ++i) {
		glutBitmapCharacter(GLUT_BITMAP_HELVETICA_18, textString[i]);
	}
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();
	glMatrixMode(GL_MODELVIEW);
	glEnable(GL_LIGHTING);
}

void printGameInfo() {
	int x = glutGet(GLUT_SCREEN_WIDTH) - 300;
	int y = glutGet(GLUT_SCREEN_HEIGHT) - 50;
	string temp;
	if (game.gameMode == GameMode::COLLECT) {
		temp = "Collected: " + to_string(game.player.itemsCollected) + " / " + to_string(game.initialNumberOfCollectibles);
		drawGLString(x, y, temp, 0.1f, 0.1f, 0.1f);
		y -= 30;
		if (!game.collectibles.empty()) {
			temp = "Time left:" + to_string(game.timeToCollect / 1000);
			drawGLString(x, y, temp, 0.1f, 0.1f, 0.1f);
		}
	}

	//fps counter, commented as is in printDebugInfo();
	//y -= 30;
	//glRasterPos2i(x, y);
	//temp = "FPS = " + to_string((int)fps);
	//drawGLString(x, y, temp, 0.1f, 0.1f, 0.1f);


}

void printDebugInfo() {

	int x = 10;
	int y = 600;
	string toPrint;
	int len;
	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_SCREEN_WIDTH), 0, glutGet(GLUT_SCREEN_HEIGHT));

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glRasterPos2i(x, y);
	toPrint = "Pos:";
	len = toPrint.length();
	for (int i = 0; i < len; ++i) {
		glutBitmapCharacter(GLUT_BITMAP_HELVETICA_18, toPrint[i]);
	}

	y -= 30;
	glRasterPos2i(x, y);
	toPrint = "x = " + to_string(game.player.pos.x);
	len = toPrint.length();
	for (int i = 0; i < len; ++i) {
		glutBitmapCharacter(GLUT_BITMAP_HELVETICA_18, toPrint[i]);
	}

	y -= 30;
	glRasterPos2i(x, y);
	toPrint = "y = " + to_string(game.player.pos.y);
	len = toPrint.length();
	for (int i = 0; i < len; ++i) {
		glutBitmapCharacter(GLUT_BITMAP_HELVETICA_18, toPrint[i]);
	}

	y -= 30;
	glRasterPos2i(x, y);
	toPrint = "z = " + to_string(game.player.pos.z);
	len = toPrint.length();
	for (int i = 0; i < len; ++i) {
		glutBitmapCharacter(GLUT_BITMAP_HELVETICA_18, toPrint[i]);
	}

	y -= 30;
	glRasterPos2i(x, y);
	toPrint = "Dir:";
	len = toPrint.length();
	for (int i = 0; i < len; ++i) {
		glutBitmapCharacter(GLUT_BITMAP_HELVETICA_18, toPrint[i]);
	}

	y -= 30;
	glRasterPos2i(x, y);
	toPrint = "x = " + to_string(game.player.dir.x);
	len = toPrint.length();
	for (int i = 0; i < len; ++i) {
		glutBitmapCharacter(GLUT_BITMAP_HELVETICA_18, toPrint[i]);
	}

	y -= 30;
	glRasterPos2i(x, y);
	toPrint = "y = " + to_string(game.player.dir.y);
	len = toPrint.length();
	for (int i = 0; i < len; ++i) {
		glutBitmapCharacter(GLUT_BITMAP_HELVETICA_18, toPrint[i]);
	}

	y -= 30;
	glRasterPos2i(x, y);
	toPrint = "z = " + to_string(game.player.dir.z);
	len = toPrint.length();
	for (int i = 0; i < len; ++i) {
		glutBitmapCharacter(GLUT_BITMAP_HELVETICA_18, toPrint[i]);
	}

	y -= 30;
	glRasterPos2i(x, y);
	toPrint = "velX = " + to_string(game.player.velX);
	len = toPrint.length();
	for (int i = 0; i < len; ++i) {
		glutBitmapCharacter(GLUT_BITMAP_HELVETICA_18, toPrint[i]);
	}

	y -= 30;
	glRasterPos2i(x, y);
	toPrint = "velY = " + to_string(game.player.velY);
	len = toPrint.length();
	for (int i = 0; i < len; ++i) {
		glutBitmapCharacter(GLUT_BITMAP_HELVETICA_18, toPrint[i]);
	}

	y -= 30;
	glRasterPos2i(x, y);
	toPrint = "velZ = " + to_string(game.player.velZ);
	len = toPrint.length();
	for (int i = 0; i < len; ++i) {
		glutBitmapCharacter(GLUT_BITMAP_HELVETICA_18, toPrint[i]);
	}

	y -= 30;
	glRasterPos2i(x, y);
	toPrint = "velRY = " + to_string(game.player.velRY);
	len = toPrint.length();
	for (int i = 0; i < len; ++i) {
		glutBitmapCharacter(GLUT_BITMAP_HELVETICA_18, toPrint[i]);
	}

	y -= 30;
	glRasterPos2i(x, y);
	toPrint = "propellerSpeed = " + to_string(game.player.propellerSpeed);
	len = toPrint.length();
	for (int i = 0; i < len; ++i) {
		glutBitmapCharacter(GLUT_BITMAP_HELVETICA_18, toPrint[i]);
	}
	y -= 30;
	glRasterPos2i(x, y);
	toPrint = "FPS = " + to_string((int)fps);
	len = toPrint.length();
	for (int i = 0; i < len; ++i) {
		glutBitmapCharacter(GLUT_BITMAP_HELVETICA_18, toPrint[i]);
	}

	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();
	glMatrixMode(GL_MODELVIEW);

}

void renderScene() {

	float emissionOff[4] = { 0.0f, 0.0f, 0.0f, 1.0f };
	float whiteEmission[4]{ 0.5f, 0.5f, 0.5f, 1.0f };

#pragma region Lighting

	if (!game.lighting.empty()) {
		int lampCounter = 0;

		for (list<Lamp>::iterator it = game.lighting.begin(); it != game.lighting.end(); ++it) {
			glLightf(GL_LIGHT0 + lampCounter, GL_LINEAR_ATTENUATION, 0.08);

			glLightfv(GL_LIGHT0 + lampCounter, GL_AMBIENT, it->amb);
			glLightfv(GL_LIGHT0 + lampCounter, GL_DIFFUSE, it->dif);
			glLightfv(GL_LIGHT0 + lampCounter, GL_SPECULAR, it->spe);
			glLightfv(GL_LIGHT0 + lampCounter, GL_POSITION, it->pos);
			lampCounter++;
		}
	}
#pragma endregion

#pragma region Player

	//render player's collision shapes
	if (debugMode) {
		if (!game.player.collisionShapes.empty()) {

			float amb[] = { 1.0f, 0.0f, 0.0f, 1.0f };
			float dif[] = { 1.0f, 0.0f, 0.0f, 1.0f };
			float spe[] = { 0.0f, 0.0f, 0.0f, 0.0f };
			glMaterialfv(GL_FRONT, GL_AMBIENT, amb);
			glMaterialfv(GL_FRONT, GL_DIFFUSE, dif);
			glMaterialfv(GL_FRONT, GL_SPECULAR, spe);
			for (list<BoundingBox>::iterator it = game.player.collisionShapes.begin(); it != game.player.collisionShapes.end(); ++it) {


				glPushMatrix();
				glTranslatef(game.player.pos.x + it->center.x, game.player.pos.y + it->center.y, game.player.pos.z + it->center.z);

				glScalef(it->size.x, it->size.y, it->size.z);
				glutWireCube(1.0f);
				glPopMatrix();
			}
		}
	}


	//player 
	float player_amb[] = { 1.0f, 1.0f, 1.0f, 1.0f };
	float player_dif[] = { 1.0f, 1.0f, 1.0f, 1.0f };
	float player_spe[] = { 0.6f, 0.6f, 0.6f, 0.6f };
	glMaterialfv(GL_FRONT, GL_AMBIENT, player_amb);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, player_dif);
	glMaterialfv(GL_FRONT, GL_SPECULAR, player_spe);
	glMaterialf(GL_FRONT, GL_SHININESS, 10.0f);


	//player-heli
	glEnable(GL_TEXTURE_2D);

	glBindTexture(GL_TEXTURE_2D, game.player.playerTexture);

	glPushMatrix();

	glTranslatef(game.player.pos.x, game.player.pos.y, game.player.pos.z);
	glRotatef(MyMath::radiansToDegrees(game.player.rotationY), 0.0f, 1.0f, 0.0f);

	glCallList(game.player.playerLoadedObject);

	glPopMatrix();

	glDisable(GL_TEXTURE_2D);

	//main propeller
	float prop_amb[] = { 0.02f, 0.02f, 0.02f, 1.0f };
	float prop_dif[] = { 0.01f, 0.01f, 0.01f, 1.0f };
	float prop_spe[] = { 0.01f, 0.01f, 0.01f, 1.0f };
	glMaterialfv(GL_FRONT, GL_AMBIENT, prop_amb);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, prop_dif);
	glMaterialfv(GL_FRONT, GL_SPECULAR, prop_spe);
	glMaterialf(GL_FRONT, GL_SHININESS, 50.0f);

	glPushMatrix();

	glTranslatef(game.player.pos.x, game.player.pos.y, game.player.pos.z);
	glRotatef(MyMath::radiansToDegrees(game.player.rotationY), 0.0f, 1.0f, 0.0f);

	glRotatef(game.player.propellerRotation, 0.0f, 1.0f, 0.0f);

	glCallList(game.player.mainPropeller);

	glPopMatrix();

	//second (side) propeller
	float prop2_amb[] = { 0.02f, 0.02f, 0.02f, 1.0f };
	float prop2_dif[] = { 0.01f, 0.01f, 0.01f, 1.0f };
	float prop2_spe[] = { 0.01f, 0.01f, 0.01f, 1.0f };
	glMaterialfv(GL_FRONT, GL_AMBIENT, prop2_amb);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, prop2_dif);
	glMaterialfv(GL_FRONT, GL_SPECULAR, prop2_spe);
	glMaterialf(GL_FRONT, GL_SHININESS, 2.0f);

	glPushMatrix();

	glTranslatef(game.player.pos.x, game.player.pos.y, game.player.pos.z);
	glRotatef(MyMath::radiansToDegrees(game.player.rotationY), 0.0f, 1.0f, 0.0f);

	//translate, as the model is in the center of coordinate system
	glTranslatef(-0.04f, 0.4f, -1.0f);
	glRotatef(game.player.propellerRotation, 1.0f, 0.0f, 0.0f);

	glCallList(game.player.secondPropeller);

	glPopMatrix();
#pragma endregion

#pragma region Game objects
	//render game objects (with their collision shapes if debugMode)
	if (!game.scene.empty()) {
		for (list<gameObject>::iterator it = game.scene.begin(); it != game.scene.end(); ++it) {

			glMaterialfv(GL_FRONT, GL_AMBIENT, it->amb);
			glMaterialfv(GL_FRONT, GL_DIFFUSE, it->dif);
			glMaterialfv(GL_FRONT, GL_SPECULAR, it->spe);
			glMaterialf(GL_FRONT, GL_SHININESS, it->shininess);

			if (it->name.compare("lamp") == 0) {
				glMaterialfv(GL_FRONT, GL_EMISSION, whiteEmission);
			}
			else {
				glMaterialfv(GL_FRONT, GL_EMISSION, emissionOff);
			}

			glPushMatrix();
			glTranslatef(it->posX, it->posY, it->posZ);
			glRotatef(it->rotation.x, 1.0f, 0.0f, 0.0f);
			glRotatef(it->rotation.y, 0.0f, 1.0f, 0.0f);
			glRotatef(it->rotation.z, 0.0f, 0.0f, 1.0f);

			if (it->texture != 0) {
				glEnable(GL_TEXTURE_2D);
				glBindTexture(GL_TEXTURE_2D, it->texture);
			}

			glCallList(it->model);

			if (it->texture != 0) {
				glDisable(GL_TEXTURE_2D);

			}
			glPopMatrix();

			//render objects' collision shapes
			if (debugMode) {
				float m1_amb[] = { 1.0f, 0.0f, 0.0f, 1.0f };
				float m1_dif[] = { 1.0f, 0.0f, 0.0f, 1.0f };
				float m1_spe[] = { 0.0f, 0.0f, 0.0f, 0.0f };

				glMaterialfv(GL_FRONT, GL_AMBIENT, m1_amb);
				glMaterialfv(GL_FRONT, GL_DIFFUSE, m1_dif);
				glMaterialfv(GL_FRONT, GL_SPECULAR, m1_spe);
				if (!it->collisionShapes.empty()) {
					for (list<BoundingBox>::iterator bbIterator = it->collisionShapes.begin(); bbIterator != it->collisionShapes.end(); ++bbIterator) {
						glPushMatrix();
						glTranslatef(it->posX + bbIterator->center.x, it->posY + bbIterator->center.y, it->posZ + bbIterator->center.z);
						glScalef(bbIterator->size.x, bbIterator->size.y, bbIterator->size.z);
						glutWireCube(1.0f);
						glPopMatrix();
					}
				}

			}

		}
	}

#pragma endregion

#pragma region Collectibles

	//render collectibles (with their collision shapes if debugMode)
	if (!(game.collectibles.empty())) {
		float emissionOn[4] = { 0.628281f, 0.555802f, 0.366065f, 1.0f };
		float cyan[4] = { 0.0f, 1.0f, 1.0f, 1.0f };
		float black[4] = { 0.0f, 0.0f, 0.0f, 0.0f };

		for (list<CollectibleObject>::iterator it = game.collectibles.begin(); it != game.collectibles.end(); ++it) {

			glMaterialfv(GL_FRONT, GL_SPECULAR, it->spe);

			if (it == game.collectibles.begin()) {

				glMaterialfv(GL_FRONT, GL_AMBIENT, black);
				glMaterialfv(GL_FRONT, GL_DIFFUSE, cyan);
				glMaterialfv(GL_FRONT, GL_EMISSION, it->amb);
				cyan[1] = 0.2f;
				cyan[2] = 0.2f;
				glMaterialfv(GL_FRONT, GL_EMISSION, cyan);

				glMaterialf(GL_FRONT, GL_SHININESS, 128.0);

			}
			else {
				glMaterialfv(GL_FRONT, GL_AMBIENT, it->amb);
				glMaterialfv(GL_FRONT, GL_DIFFUSE, it->dif);
				glMaterialf(GL_FRONT, GL_SHININESS, it->shininess);
				glMaterialfv(GL_FRONT, GL_EMISSION, black);
			}

			glPushMatrix();

			glTranslatef(it->posX, it->posY, it->posZ);
			glRotatef(MyMath::radiansToDegrees(game.player.rotationY), 0.0f, 1.0f, 0.0f);

			glCallList(it->model);
			glPopMatrix();

			//render object's collision shape
			if (debugMode) {
				float m1_amb[] = { 1.0f, 0.0f, 0.0f, 1.0f };
				float m1_dif[] = { 1.0f, 0.0f, 0.0f, 1.0f };
				float m1_spe[] = { 0.0f, 0.0f, 0.0f, 0.0f };
				glMaterialfv(GL_FRONT, GL_AMBIENT, m1_amb);
				glMaterialfv(GL_FRONT, GL_DIFFUSE, m1_dif);
				glMaterialfv(GL_FRONT, GL_SPECULAR, m1_spe);

				glPushMatrix();
				glTranslatef(it->posX + it->boundingBox.center.x, it->posY + it->boundingBox.center.y, it->posZ + it->boundingBox.center.z);
				glScalef(it->boundingBox.size.x, it->boundingBox.size.y, it->boundingBox.size.z);
				glutWireCube(1.0f);
				glPopMatrix();

			}
		}

		glMaterialfv(GL_FRONT, GL_EMISSION, emissionOff);

	}

#pragma endregion
	


}

void renderMenu() {


	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	gluLookAt(
		0.0f, 0.0f, 0.0f, // Pozycja kamery
		0.0f, 0.0f, 1.0f, // Punkt na ktory patrzy kamera (pozycja + kierunek)
		0.0f, 1.0f, 0.0f // Wektor wyznaczajacy pion
	);

	// SEE
	//enter2dMode();
	leave2dMode();

	float offsetZ = 51.0f;

	//render background
	glBegin(GL_QUADS);
	//float beige[4] = { 0.698f, 0.56f, 0.3f, 1.0f };
	//float beige[4] = { 1.0f, 0.87f, 0.635f, 1.0f };
	float background[4] = { 0.1f, 0.1f, 0.1f, 1.0f };
	glMaterialfv(GL_FRONT, GL_AMBIENT, background);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, background);
	glMaterialfv(GL_FRONT, GL_SPECULAR, background);



	glNormal3f(0.0f, 0.0f, -1.0f);
	glVertex3f(-100.0, 100.0, offsetZ);
	glVertex3f(100.0, 100.0, offsetZ);
	glVertex3f(100.0, -100.0, offsetZ);
	glVertex3f(-100.0, -100.0, offsetZ);

	glEnd();

	//render buttons
	offsetZ -= 1.0f;
	for (list<Button>::iterator bi = menu.buttons.begin(); bi != menu.buttons.end(); ++bi) {

		glBegin(GL_QUADS);
		glMaterialfv(GL_FRONT, GL_AMBIENT, bi->color);
		glMaterialfv(GL_FRONT, GL_DIFFUSE, bi->color);
		glMaterialfv(GL_FRONT, GL_DIFFUSE, bi->color);

		if (bi->isSelected) {
			glMaterialfv(GL_FRONT, GL_AMBIENT, bi->colorOfSelected);
			glMaterialfv(GL_FRONT, GL_DIFFUSE, bi->colorOfSelected);
		}

		glNormal3f(0.0f, 0.0f, -1.0f);
		glVertex3f(bi->leftX, bi->topY, offsetZ);
		glVertex3f(bi->rightX, bi->topY, offsetZ);
		glVertex3f(bi->rightX, bi->bottomY, offsetZ);
		glVertex3f(bi->leftX, bi->bottomY, offsetZ);

		glEnd();

		drawGLString(glutGet(GLUT_SCREEN_WIDTH)*0.5f + bi->posX * 10.0f - bi->text.length()*10.0f, glutGet(GLUT_SCREEN_HEIGHT)*0.5f + bi->posY * 18.0f,
			bi->text,
			1.0f, 1.0f, 1.0f);

	}

	glFlush();
	glutSwapBuffers();
	glutPostRedisplay();
}

void onRender() {

	currentTime = glutGet(GLUT_ELAPSED_TIME);
	deltaTime = currentTime - oldTime;
	oldTime = currentTime;
	// fps is average of last and current value:
	fps = ((1000.0f / (float)deltaTime ) + fps) * 0.5f ;

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	game.camera.updateCamera(game.player);

	game.handleCollisions();

	gluLookAt(
		game.camera.pos.x, game.camera.pos.y, game.camera.pos.z,
		game.camera.pos.x + game.camera.dir.x, game.camera.pos.y + game.camera.dir.y, game.camera.pos.z + game.camera.dir.z,
		0.0f, 1.0f, 0.0f
	);

	if (debugMode) {
		printDebugInfo();
	}

	if (!toPrintAtCenter.empty()) {
		drawGLString(600, 400, toPrintAtCenter, 1.0f, 1.0f, 1.0f);
	}

	printGameInfo();
	renderScene();

	glutSwapBuffers();
	//glFlush();
	//without glFinish(), game works faster in debugMode (to be more specific: when printing debugInfo) !! (sic!)
	//glFinish();
	glutPostRedisplay();

}

void onReshape(int width, int height) {
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glViewport(0, 0, width, height);
	gluPerspective(60.0f, (float)width / height, .01f, 100.0f);
}

vec3 loadSettingsReturnResolution() {

	string path = "res\\settings\\settings.ini";
	ifstream input(path);
	vec3 resolution;
	resolution.x = 1024;
	resolution.y = 768;
	resolution.z = 0;

	for (string line; getline(input, line); )
	{
		if (line.compare("width =") == 0) {
			getline(input, line);
			string::size_type sz;
			resolution.x = stof(line, &sz);
		}
		else if (line.compare("height =") == 0) {
			getline(input, line);
			string::size_type sz;
			resolution.y = stof(line, &sz);
		}
	}
	return resolution;

}

void handleButton(Button button)
{
	if (button.buttonType == ButtonType::NEW_GAME) {
		//initGame("livingRoom.lvl","heli1.ini",GameMode::COLLECT);
		menu.loadButtons("levels.m");
	}
	else if (button.buttonType == ButtonType::QUIT) {
		glutLeaveMainLoop();
	}
	else if (button.buttonType == ButtonType::CHOOSE_LEVEL) {
		//button.text is text displayed, e.g. "Living room"
		game.levelFilename = menu.getLevelFilename(button.text);
		menu.loadButtons(game.levelFilename);
	}
	else if (button.buttonType == ButtonType::CHOOSE_GAMEMODE) {
		game.gameMode = game.getGameModeFromString(button.text);
		menu.loadStartButton();
	}
	else if (button.buttonType == ButtonType::START) {
		toPrintAtCenter = "LOADING... PLEASE WAIT";
		glutTimerFunc(3000, clearStringPrintedAtCenter, 0);
		initGame(game.levelFilename, "heli1.ini", game.gameMode);
	}
	
}

void leave2dMode() {
	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();
}

void enter2dMode()
{
	/*
	* Change to the projection matrix and set our viewing volume.
	*/
	glMatrixMode(GL_PROJECTION);
	glPushMatrix();

	/*
	* Reset the view
	*/
	glLoadIdentity();

	/*
	* 2D projection
	*/
	//glOrtho(0, glutGet(GLUT_SCREEN_WIDTH),
	//	glutGet(GLUT_SCREEN_HEIGHT), 0,
	//	9999.0, -9999.0);

	// ^^^ niepotrzebne, bo najwyrazniej wystarczy ortho2d :
	// SEE
	gluOrtho2D(0, glutGet(GLUT_SCREEN_WIDTH), 0, glutGet(GLUT_SCREEN_HEIGHT));

	/*
	* Make sure we're changing the model view and not the projection
	*/
	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();

	/*
	* Reset the view
	*/
	glLoadIdentity();
}

void initGame(string levelFilename, string playerFilename, GameMode gameMode) {
	if (generalState != GeneralState::GAME) {
		//leave2dMode();

		//https://stackoverflow.com/questions/2166099/calling-a-constructor-to-re-initialize-object
		new(&game) Game();

		game.camera = Camera();

		//inicjalizacja kamery
		//poczatkowo kamera ma ten sam kierunek/pozycje co gracz
		game.camera.pos.x = game.player.pos.x;
		game.camera.pos.y = game.player.pos.y;
		game.camera.pos.z = game.player.pos.z;

		game.camera.dir.x = game.player.dir.x;
		game.camera.dir.y = game.player.dir.y;
		game.camera.dir.z = game.player.dir.z;

		// SEE dlaczego wczesniej korzystamy z pol obiektu game.player, a dopiero jest inicjalizacja?
		game.player = Player();

		game.loadLevel(levelFilename);
		game.player.setPlayer(playerFilename);
		if (gameMode == GameMode::COLLECT) {
			game.loadCollectibles(game.collectiblesFilename);
		}
		game.loadLightSources(game.lightsFilename);
		game.gameMode = gameMode;
		
		if (game.fogActive) {
			glFogi(GL_FOG_MODE, GL_EXP2);
			float greyish[4] = { 0.8f, 0.8f, 0.8f, 1.0f };
			glFogfv(GL_FOG_COLOR, greyish);
			glFogf(GL_FOG_DENSITY, 0.1f);
			glHint(GL_FOG_HINT, GL_DONT_CARE);
			glFogf(GL_FOG_START, 1.0f);
			glFogf(GL_FOG_END, 30.0f);
			glEnable(GL_FOG);
		}


		glClearColor(0.1f, 0.1f, 0.1f, 1.0f);

		float gl_amb[] = { 0.0f, 0.0f, 0.0f, 1.0f };
		glLightModelfv(GL_LIGHT_MODEL_AMBIENT, gl_amb);

		glEnable(GL_LIGHTING);

		for (int i = 0; i < game.lighting.size(); i++) {
			glEnable(GL_LIGHT0 + i);
		}

		glutKeyboardFunc(gameOnKeyPress);
		glutDisplayFunc(onRender);
		glutTimerFunc(17, onTimer, 0);
		glutTimerFunc(100, tenthOfSec, 0);
		generalState = GeneralState::GAME;
	}
}

void initMenu() {
	if (generalState != GeneralState::MENU) {
		float gl_amb[] = { 1.0f, 1.0f, 1.0f, 1.0f };
		glLightModelfv(GL_LIGHT_MODEL_AMBIENT, gl_amb);
		
		//enter2dMode();

		glDisable(GL_LIGHTING);
		if (!game.lighting.empty()) {
			for (int i = 0; i < game.lighting.size(); i++) {
				glDisable(GL_LIGHT0 + i);
			};
		}
		glDisable(GL_FOG);

		game.~Game();

		menu.loadButtons("main.m");

		glutKeyboardFunc(menuOnKeyPress);
		glutSpecialFunc(menuSpecialKeyPress);
		glutDisplayFunc(renderMenu);

		generalState = GeneralState::MENU;
	}
}

void clearStringPrintedAtCenter(int id) {
	toPrintAtCenter.clear();
}