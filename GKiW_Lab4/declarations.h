#pragma once

#define PI 3.14159265
#define ESC 27
#define ENTER 13


// Deklaracje funkcji u�ywanych jako obs�uga zdarze� GLUT-a.
void onRender();
void renderMenu();

void onReshape(int, int);
void gameOnKeyPress(unsigned char, int, int);
void gameOnKeyDown(unsigned char, int, int);

void menuOnKeyPress(unsigned char, int, int);
void menuOnKeyDown(unsigned char, int, int);
void menuSpecialKeyPress(int key, int x, int y);

void onKeyUp(unsigned char, int, int);
void OnMouseMove(int, int);
void onTimer(int);

void enter2dMode();
void leave2dMode();

struct vec3 {
	float x, y, z;
};