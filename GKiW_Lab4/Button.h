#pragma once

#include <string>

#include "ButtonType.h"

using namespace std;

class Button
{
public:
	float posX;
	float posY;

	float width;
	float height;

	float leftX;
	float rightX;
	float topY;
	float bottomY;

	string text;
	float color[4];
	float colorOfSelected[4];

	bool isSelected;

	ButtonType buttonType;

	Button(float posX, float posY, string caption, ButtonType buttonType);
	Button();
	~Button();
};

