#include "stdafx.h"
#include "Player.h"
#include "ObjLoader.h"
#include <string>
#include <fstream>

Player::Player()
{
	itemsCollected = 0;
	startingPos.x = 0.0f;
	startingPos.y = 0.0f;
	startingPos.z = 0.0f;

	startingDir.x = 0.0f;
	startingDir.y = 0.0f;
	startingDir.z = 1.0f;

	velRY = 0.0f;
	velZ = 0.0f;
	velX = 0.0f;
	velY = 0.0f;
}

Player::~Player()
{
}

void Player::setPlayer(string playerSettingsFilename)
{

	loadPlayerInfo(playerSettingsFilename);
	this->pos.x = this->startingPos.x;
	this->pos.y = this->startingPos.y;
	this->pos.z = this->startingPos.z;

	this->dir.x = this->startingDir.x;
	this->dir.y = this->startingDir.y;
	this->dir.z = this->startingDir.z;

}

void Player::updateBoundingBoxesPositions() {
	for (list<BoundingBox>::iterator bbIterator = this->collisionShapes.begin(); bbIterator != this->collisionShapes.end(); ++bbIterator) {
		bbIterator->setPos(bbIterator->center.x + this->pos.x, bbIterator->center.y + this->pos.y, bbIterator->center.z + this->pos.z);
	}
}

void Player::handleChanges()
{
	// Obr�t kamery (wsp. sferyczne):
	float T = acos(this->dir.y);
	float G = atan2(this->dir.z, this->dir.x);
	//T -= this->velRX * .03f;
	G += this->velRY*0.01f;
	this->dir.x = sin(T) * cos(G);
	this->dir.y = cos(T);
	this->dir.z = sin(T) * sin(G);

	// Wektor prostopad�y (per - perpendicular):
	vec3 per;
	per.x = -this->dir.z;
	per.y = 0;
	per.z = this->dir.x;

	// Ruch przod/tyl:
	this->pos.x += this->dir.x * this->velZ * .1f;

	this->pos.z += this->dir.z * this->velZ * .1f;

	// Ruch na boki:
	this->pos.x += per.x * this->velX * .1f;

	// Ruch gora/dol:
	this->pos.y += this->velY * 0.1f;

	this->pos.z += per.z * this->velX * .1f;

	// Inercja:
	//this->velRX * 0.8f;
	this->velRY = this->velRY * 0.96f;
	this->velZ = this->velZ * 0.99f;
	this->velX = this->velX * 0.99f;
	this->velY = this->velY * 0.99f;

	//gravity:
	this->simulateGravity();

	this->calculateRotationY();
	this->calculatePropellerRotation();
	//this->updateBoundingBoxesPositions();
}

void Player::calculateRotationY() {
	this->rotationY = atan2(this->dir.x, this->dir.z);

}

void Player::increaseVelX() {

	if (this->velX < this->maxVelX) {
		this->velX += this->deltaVelX;
	}
}

void Player::decreaseVelX() {
	if (this->velX > 0 || abs(this->velX) < this->maxVelX) {
		this->velX -= this->deltaVelX;
	}
}

void Player::increaseVelZ() {
	if (this->velZ < this->maxVelZ) {
		this->velZ += this->deltaVelZ;
	}
}

void Player::decreaseVelZ() {
	if (this->velZ > 0 || abs(this->velZ) < this->maxVelZ) {
		this->velZ -= this->deltaVelZ;
	}
}

void Player::increaseVelY() {
	if (this->velY < this->maxVelY) {
		this->velY += this->deltaVelY;
	}
	else {
		this->velY = this->maxVelY;
	}
}

void Player::decreaseVelY() {
	if (this->velY > this->maxVelYDown) {
		this->velY -= this->deltaVelY;
	}
}

void Player::increaseVelRY() {
	if (this->velRY < this->maxVelRY) {
		this->velRY += deltaVelRY;
	}
}

void Player::decreaseVelRY() {
	if (this->velRY > 0 || abs(this->velRY) < this->maxVelRY) {
		this->velRY -= this->deltaVelRY;
	}
}

void Player::calculatePropellerRotation() {

	this->propellerSpeed = sqrt(pow(this->velX, 2) + pow(this->velY, 2) + pow(this->velZ, 2)) * 10;


	if (this->propellerSpeed >= this->propellerMaxSpeed) this->propellerSpeed = this->propellerMaxSpeed;
	if (this->propellerSpeed <= this->propellerMinSpeed) this->propellerSpeed = 0.0f;

	this->propellerRotation += this->propellerSpeed * 10;
	if (this->propellerRotation == 360) this->propellerRotation = 0;
}

void Player::simulateGravity() {
	if (this->velY > this->maxVelYDown) {
		this->velY -= 0.01f;
	}
}

void Player::extractAndSetVelocities(string line) {
	float maxVelX, maxVelZ, maxVelY, maxVelYDown, maxVelRY, deltaVelX, deltaVelZ, deltaVelY, deltaVelRY;

	string::size_type sz;

	maxVelX = stof(line, &sz);
	line = line.substr(sz);
	sz = 0;

	maxVelZ = stof(line, &sz);
	line = line.substr(sz);
	sz = 0;

	maxVelY = stof(line, &sz);
	line = line.substr(sz);
	sz = 0;

	maxVelYDown = stof(line, &sz);
	line = line.substr(sz);
	sz = 0;

	maxVelRY = stof(line, &sz);
	line = line.substr(sz);
	sz = 0;

	deltaVelX = stof(line, &sz);
	line = line.substr(sz);
	sz = 0;

	deltaVelZ = stof(line, &sz);
	line = line.substr(sz);
	sz = 0;

	deltaVelY = stof(line, &sz);
	line = line.substr(sz);
	sz = 0;

	deltaVelRY = stof(line, &sz);

	this->maxVelX = maxVelX;
	this->maxVelY = maxVelY;
	this->maxVelZ = maxVelZ;
	this->maxVelYDown = maxVelYDown;
	this->maxVelRY = maxVelRY;
	this->deltaVelX = deltaVelX;
	this->deltaVelZ = deltaVelZ;
	this->deltaVelY = deltaVelY;
	this->deltaVelRY = deltaVelRY;

}

void Player::loadPlayerInfo(string fileName) {
	BoundingBox bb;

	string path = "res\\settings\\" + fileName;
	ifstream input(path);

	ObjLoader objLoader;
	string temp;

	for (string line; getline(input, line); )
	{
		if (line.at(0) == '#') {
			continue;
		}
		else if (line.compare("bounding box:") == 0) {
			getline(input, line);
			this->collisionShapes.push_back(BoundingBox::createBoundingBoxFromString(line));
		}
		else if (line.compare("model =") == 0) {
			getline(input, line);
			temp = "res\\models\\" + line;
			this->playerLoadedObject = objLoader.LoadObj(temp.c_str());
		}
		else if (line.compare("texture =") == 0) {
			getline(input, line);
			temp = "res\\textures\\" + line;
			this->playerTexture = objLoader.LoadTexture(temp.c_str());
		}
		else if (line.compare("mainPropeller =") == 0) {
			getline(input, line);
			temp = "res\\models\\" + line;
			this->mainPropeller = objLoader.LoadObj(temp.c_str());
		}
		else if (line.compare("secondPropeller =") == 0) {
			getline(input, line);
			temp = "res\\models\\" + line;
			this->secondPropeller = objLoader.LoadObj(temp.c_str());
		}
		else if (line.compare("velocities:") == 0) {
			getline(input, line);
			extractAndSetVelocities(line);
		}
	}
}

void Player::reset() {
	this->pos.x = this->startingPos.x;
	this->pos.y = this->startingPos.y;
	this->pos.z = this->startingPos.z;

	this->dir.x = this->startingDir.x;
	this->dir.y = this->startingDir.y;
	this->dir.z = this->startingDir.z;

	velRY = 0.0f;
	velZ = 0.0f;
	velX = 0.0f;
	velY = 0.0f;

}