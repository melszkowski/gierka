#pragma once

#include <string>

using namespace std;

class BoundingBox
{
public:
	vec3 center;
	vec3 size;
	bool areLegs = false;
	BoundingBox();
	BoundingBox(float posX, float posY, float posZ, float width, float height, float depth);
	~BoundingBox();
	static BoundingBox createBoundingBoxFromString(string line);
	void setPos(float posX, float posY, float posZ);
};

