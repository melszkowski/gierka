#pragma once
#include <vector>
#include "stdafx.h"
#include <gl\GL.h>

class ObjLoader
{
public:


	ObjLoader();
	~ObjLoader();
	GLuint LoadObj(const char * file);
	GLuint LoadTexture(const char * filename);
};

