#pragma once
enum class GeneralState {
	NONE = 0,
	MENU = 1,
	GAME = 2
};